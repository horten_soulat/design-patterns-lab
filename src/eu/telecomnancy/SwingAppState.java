package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.patterns.state.StateTemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

//test du pattern State 
public class SwingAppState {

    public static void main(String[] args) {
        ISensor sensor = new StateTemperatureSensor();
        new MainWindow(sensor);
    }
	
}
