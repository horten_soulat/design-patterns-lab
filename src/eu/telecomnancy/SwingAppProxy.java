package eu.telecomnancy;

import eu.telecomnancy.patterns.proxy.Proxy;

import eu.telecomnancy.ui.MainWindow;

public class SwingAppProxy {

    public static void main(String[] args) {
        Proxy proxy = new Proxy();
        new MainWindow(proxy);
    }
	
}
