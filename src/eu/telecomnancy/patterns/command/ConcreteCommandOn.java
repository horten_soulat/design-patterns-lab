package eu.telecomnancy.patterns.command;

import eu.telecomnancy.sensor.ISensor;

public class ConcreteCommandOn implements Command {

	ISensor reicever;

	public ConcreteCommandOn(ISensor reicever) {
		this.reicever = reicever;
	}

	@Override
	public Object execute() throws Exception {
		this.reicever.on();
		return null;
	}	
}
