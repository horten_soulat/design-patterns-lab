package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.patterns.AdapterLegacyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
        //ISensor sensor = new TemperatureSensor();
    	LegacyTemperatureSensor lts = new LegacyTemperatureSensor();
    	ISensor sensor =  new AdapterLegacyTemperatureSensor(lts);
    	new ConsoleUI(sensor);
    }

}