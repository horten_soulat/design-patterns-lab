package eu.telecomnancy;

import eu.telecomnancy.patterns.factory.ConcreteCreator;
import eu.telecomnancy.patterns.factory.Creator;
import eu.telecomnancy.sensor.ISensor;

import eu.telecomnancy.ui.ConsoleUI;

public class AppFactory {

	public static void main(String[] args) {
        Creator c = new ConcreteCreator();
    	ISensor sensor =  c.factoryMethode("Legacy");
    	new ConsoleUI(sensor);
    }

}
