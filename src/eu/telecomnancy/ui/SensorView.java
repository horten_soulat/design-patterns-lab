package eu.telecomnancy.ui;

import eu.telecomnancy.patterns.command.Command;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;

import com.sun.webkit.Invoker;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observer;
import java.util.Observable;

public class SensorView extends JPanel implements Observer {
    private ISensor sensor;
    private Command ConcreteCommandOn;
    private Command ConcreteCommandOff;
    private Command ConcreteCommandUpdate;
    private Command ConcreteCommandGetValue;
    private Invoker invoker;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(ISensor c) {
        this.sensor = c;
      //  this.ConcreteCommandOn = new ConcreteCommandOn(c);
      //  this.ConcreteCommandOff = new ConcreteCommandOff(c);
      //  this.ConcreteCommandUpdate = new ConcreteCommandUpdtae(c);
      //  this.ConcreteCommandUpdate = new ConcreteCommandGetValue(c);
      //  this.invoker = new Invoker();
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
        //        invoker.execute(ConcreteCommandOn);
            	sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//              invoker.execute(ConcreteCommandOff);
            	sensor.off();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
//                  invoker.execute(ConcreteCommandUpdate);
                    sensor.update();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    //ajout pour observer
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg0 instanceof ISensor){
			sensor = (ISensor) arg0;
			try{
//		        value.setText(invoker.execute(ConcreteCommandGetValue)+" ");
				value.setText(sensor.getValue()+" ");
				repaint();
			}catch(SensorNotActivatedException e){
				e.printStackTrace();
			}
		}
		
	}
    
    
}
