package eu.telecomnancy.patterns.command;

public class Invoker {

	public Invoker(){
		
	}
	
	public Object execute(Command cmd) throws Exception{
		return cmd.execute();
	}
	
}
