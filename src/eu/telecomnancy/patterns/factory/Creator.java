package eu.telecomnancy.patterns.factory;

import eu.telecomnancy.sensor.ISensor;

public interface Creator {

	public ISensor factoryMethode();
	
	public ISensor factoryMethode(String type);
	
}
