package eu.telecomnancy.patterns.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class ConcreteDecoratorArrondit extends Decorator {

	public ConcreteDecoratorArrondit(ISensor decorated) {
		super(decorated);
	}

	public double getValue() throws SensorNotActivatedException {
		return Math.round(this.decorated.getValue());
	}

}
