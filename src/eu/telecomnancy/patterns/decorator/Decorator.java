package eu.telecomnancy.patterns.decorator;

import java.util.Observable;
import java.util.Observer;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class Decorator extends Observable implements ISensor {

	ISensor decorated;
	
	public Decorator(ISensor decorated) {
		this.decorated = decorated;
	}

	@Override
	public void on() {
		this.decorated.on();
	}

	@Override
	public void off() {
		this.decorated.off();		
	}

	@Override
	public boolean getStatus() {
		return this.decorated.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.decorated.update();
	}


	@Override
	public synchronized void addObserver(Observer o) {
		((Observable)this.decorated).addObserver(o);
	}


	
}
