package eu.telecomnancy.patterns.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Context {

	State state;
	
	public Context() {
		this.state = new StateOff();
	}

	public void on() {
		this.state = new StateOn();
	}

	public void off() {
		this.state = new StateOff();
	}

	public boolean getStatus() {
		return this.state.getStatus();
	}

	public void setValue(double d) throws SensorNotActivatedException{
		this.state.setValue(d);
	}

	public double getValue() throws SensorNotActivatedException {
		return this.state.getValue();
	}
}
