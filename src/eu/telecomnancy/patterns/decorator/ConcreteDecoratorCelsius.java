package eu.telecomnancy.patterns.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class ConcreteDecoratorCelsius extends Decorator {

	
	public ConcreteDecoratorCelsius(ISensor decorated) {
		super(decorated);
	}

	public double getValue() throws SensorNotActivatedException {
		return this.decorated.getValue();
	}

}
