package eu.telecomnancy.patterns.factory;

import eu.telecomnancy.patterns.AdapterLegacyTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;

public class ConcreteCreator implements Creator {

	@Override
	public ISensor factoryMethode() {
		return new TemperatureSensor();
	}

	@Override
	public ISensor factoryMethode(String type) {
		
		if (type.equals("Default"))
			return new TemperatureSensor();
		else if(type.equals("Legacy")){
			LegacyTemperatureSensor lts = new LegacyTemperatureSensor();
		    return new AdapterLegacyTemperatureSensor(lts);
		}
		else
			return factoryMethode();
	}

}
