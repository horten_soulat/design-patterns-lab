package eu.telecomnancy.patterns.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class ConcreteDecoratorFahrenheit extends Decorator {

	public ConcreteDecoratorFahrenheit(ISensor decorated) {
		super(decorated);
	}

	public double getValue() throws SensorNotActivatedException {
		System.out.println("F");
		return 1.8*this.decorated.getValue() + 32;
		
	}

	
}
