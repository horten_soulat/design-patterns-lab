package eu.telecomnancy;

import eu.telecomnancy.patterns.decorator.ConcreteDecoratorCelsius;
import eu.telecomnancy.patterns.decorator.ConcreteDecoratorFahrenheit;
import eu.telecomnancy.patterns.decorator.Decorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppDecorated {

public static void main(String[] args) {
        
		ISensor sensor = new TemperatureSensor();
    	Decorator decorated = new ConcreteDecoratorFahrenheit(sensor);
    	new ConsoleUI(decorated);
    }
	
}
