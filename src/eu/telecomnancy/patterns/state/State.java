package eu.telecomnancy.patterns.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class State {

	double value;	
	public abstract void on(Context c);
	public abstract void off(Context c);
	public abstract boolean getStatus();
	public abstract double getValue() throws SensorNotActivatedException;
	public abstract void setValue(double value) throws SensorNotActivatedException;
}
